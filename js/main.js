
$(".btn-modal").fancybox({
    'padding'    : 0
});


ymaps.ready(init);
var myMap;

function init() {
    myMap = new ymaps.Map("map", {
        center: [55.668878569081066,37.55069249999998],
        controls: ['zoomControl'],
        zoom: 15
    }, {
        searchControlProvider: 'yandex#search'
    });

    myMap.geoObjects

        .add(new ymaps.Placemark([55.668878569081066,37.55069249999998], {
            balloonContent: 'Москва, ул. Профсоюзная 58, корп. 4'
        }, {
            preset: 'islands#icon',
            iconColor: '#0095b6'
        }))

        .add(new ymaps.Placemark([55.773672068963116,37.67698699999992], {
            balloonContent: 'Москва, Спартаковская улица, 19, строение 3'
        }, {
            preset: 'islands#icon',
            iconColor: '#0095b6'
        }))

        .add(new ymaps.Placemark([55.765088568970874,37.55754699999992], {
            balloonContent: 'Москва, ул. Б. Декабрьская, 3, строение 1'
        }, {
            preset: 'islands#icon',
            iconColor: '#0095b6'
        }))

        .add(new ymaps.Placemark([59.932261064156265,30.349222500000014], {
            balloonContent: 'Санкт Петербург, Невский проспект, 51'
        }, {
            preset: 'islands#icon',
            iconColor: '#0095b6'
        }));
}

$('.contact__nav > li > a').on('click touchstart', function(e){
    e.preventDefault();

    var x = $(this).attr("data-x");
    var y = $(this).attr("data-y");
    console.log(x);
    console.log(y);

    $(this).closest('.contact__nav').find('li').removeClass('active');
    $(this).closest('li').addClass('active');
    myMap.setCenter([x, y], 15, {
        checkZoomRange: true
    });
});
